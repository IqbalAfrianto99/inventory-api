const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");
const morgan = require("morgan");
var io = require('socket.io');


const Barang = require("./routes/api/Barang");
const Ruangan = require("./routes/api/Ruangan");
const Mutasi = require("./routes/api/Mutasi");
const User = require("./routes/api/User");

const app = express();

//logger
app.use(morgan('dev'));
// Body parser middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// DB Config
const db = require("./config/key").mongoURI;

// Connect to MongoDB
mongoose
  .connect(db, { useNewUrlParser: true, useFindAndModify: false })
  .then(() => {
    console.log("Mantap slur");
    console.log("MongoDB Connected");
  })
  .catch(err => {
    console.log(err);
  });

// Passport middleware
app.use(passport.initialize());

// Passport Config
require("./config/passport")(passport);

// Use Routes
app.use("/api/barang", Barang);
app.use("/api/ruang", Ruangan);
app.use("/api/mutasi", Mutasi);
app.use("/api/user", User);

const port = process.env.PORT || 8080;

const server = app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});

const socket = io(server);

socket.on('connection', (socket) => {
  console.log('1 client connected');

  socket.on('disconnect', function(){
    console.log('1 client disconnected');
  });
  socket.on('permohonan', (permohonans) => {
    console.log('Permohonan baru dibuat: ', permohonans);
  });

  socket.on('channel1', (data) => {
    console.log('Greetings from RN app', data);
  });

})
