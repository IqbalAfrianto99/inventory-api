const mongoose = require("mongoose");
const Schema = mongoose.Schema;

/**
 * Ruang models
 * @todo Sebagai schema dalam pembuatan object di datatabase
 *
 * @property {ObjectId} id   - id ruang di dalam database
 * @property {String} nama - nama ruang di database
 */

const RuanganSchema = new Schema({
  nama: {
    type: String,
    required: true
  }
});

module.exports = Ruangan = mongoose.model("ruangan", RuanganSchema);
