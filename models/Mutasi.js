const mongoose = require("mongoose");
const Schema = mongoose.Schema;

/**
 * Mutasi models
 * @todo Sebagai schema dalam pembuatan object di datatabase
 *
 * @property {ObjectId} id        - id barang di dalam database
 * @property {Date} tgl           - Tanggal dibuatnya mutasi
 * @property {Array} item         - barang yang dimohon
 * @property {ObjectId} barang    - id barang yang dimohon ( bagian dari array item )
 * @property {Number} qty_mohon   - jumlah barang yang dimohon ( bagian dari array item )
 * @property {Number} qty_ambil   - jumlah barang yang dikonfirmasi ( bagian dari array item )
 * @property {Number} qty_terima  - jumlah barang yang diterima ( bagian dari array item )
 * @property {ObjectId} user      - id user yang memohon
 * @property {String} status      - status permohonan
 */

const MutasiSchema = new Schema({
  tgl: {
    type: Date,
    default: Date.now
  },

  item: [
    {
      barang: {
        type: Schema.Types.ObjectId,
        ref: "barang"
      },

      qty_mohon: {
        type: Number,
        required: true
      },

      qty_ambil: {
        type: Number,
        default: 0
      },

      qty_terima: {
        type: Number,
        default: 0
      }
    }
  ],

  user: {
    type: Schema.Types.ObjectId,
    ref: "user"
  },

  status: {
    type: String,
    required: true,
    default: "Menunggu Konfirmasi"
  }
});

module.exports = Mutasi = mongoose.model("mutasi", MutasiSchema);
