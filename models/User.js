const mongoose = require("mongoose");
const Schema = mongoose.Schema;

/**
 * User models
 * @todo Sebagai schema dalam pembuatan object di datatabase
 *
 * @property {ObjectId} id      - id User di dalam database
 * @property {String} nama      - nama User
 * @property {String} email     - email User
 * @property {String} password  - password user
 * @property {String} role      - Role user
 */

const UserSchema = new Schema({
  nama: {
    type: String,
    required: true
  },

  email: {
    type: String,
    required: true
  },

  password: {
    type: String,
    required: true
  },

  role: {
    type: String,
    required: true
  }
});

module.exports = User = mongoose.model("user", UserSchema);
