const mongoose = require("mongoose");
const Schema = mongoose.Schema;

/**
 * Barang models
 * @todo Sebagai schema dalam pembuatan object di datatabase
 *
 * @property {ObjectId} id   - id barang di dalam database
 * @property {String} nama   - nama barang
 * @property {Number} qty    - jumlah barang
 * @property {String} satuan - satuan barang
 */

const BarangSchema = new Schema({
  nama: {
    type: String,
    required: true
  },

  qty: {
    type: Number
  },

  satuan: {
    type: String,
    required: true
  }
});

module.exports = Barang = mongoose.model("barang", BarangSchema);
