// Require barang models
const Barang = require("../../models/Barang");

/**
 * List barang dari database
 * @todo Mengambil seluruh data di dalam datatabase
 *
 * @example
 * API : GET api/barang
 *
 * @returns {Array} Returns Data barang dari dalam database
 */

// @desc   Mendapat semua barang
// @access Public
exports.getBarang = function(req, res) {
  Barang.find()
    .then(barang => {
      res.json(barang);
    })
    .catch(err => {
      res.status(404).json({ msg: "Tidak ada barang yang ditampilkan" });
    });
};

/**
 * menampilkan detail barang
 * @todo Menampilkan detail barang sesuai dengan id yang diinput
 *
 * @example
 * API : GET api/barang/detail_barang/:id
 * Example : GET api/barang/detail_barang/5cff254caf501920a048b309
 *
 * @param {ObjectId} id - id barang yang akan dicari detailnya
 */

// @desc   Mencari barang berdasarkan Id
// @access Public
exports.getDetailBarang = function(req, res) {
  Barang.findById(req.params.id)
    .then(barang => {
      res.json(barang);
    })
    .catch(err => {
      res.status(404).json({ msg: "Barang tidak ditemukan" });
    });
};

/**
 * Mencari barang berdasarkan nama
 * @todo Mencari Barang sesuai dengan nama yang diinputkan
 *
 * @example
 * API : GET api/barang/cari_barang?nama_barang=nama_barang
 * Example : GET api/barang/cari_barang?nama_barang=Parasetamol 500
 *
 * @param {String} nama_barang - nama barang yang akan dicari
 */

// @desc   Mencari barang berdasarkan Inputan (nama)
// @access Public
exports.cariBarang = function(req, res) {
  Barang.find({ nama: new RegExp(req.query.nama_barang, "i") })
    .then(barang => {
      res.json(barang);
    })
    .catch(err => {
      res.status(404).json({ msg: "Barang tidak ditemukan" });
    });
};

/**
 * Post Barang
 * @todo Memasukkan barang baru ke dalam database
 *
 * @example
 * API : POST api/Barang
 * Example : POST api/barang
 */
// Tambahan
// @desc   Memasukkan barang baru ( dummy )
exports.postBarang = function(req, res) {
  const newBarang = new Barang({
    nama: "Parasetamol 500",
    qty: 200,
    satuan: "Botol"
  });

  newBarang
    .save()
    .then(barang => res.json(barang))
    .catch(err => {
      res.status(404).json({ msg: "Gagal menyimpan barang" });
    });
};
