// Require ruang models
const Ruangan = require("../../models/Ruangan");

/**
 * List data ruangan
 * @todo Mengambil seluruh data ruangan di dalam database
 *
 * @example
 * API : GET api/ruang
 */

// @desc   Menampilkan seluruh ruangan
// @access Public
exports.getRuangan = function(req, res) {
  Ruangan.find()
    .then(ruang => {
      res.json(ruang);
    })
    .catch(err => {
      res.status(404).json({ msg: "Ruangan kosong" });
    });
};

/**
 * Mencari ruangan yang sesuai dengan nama ruang yang diinputkan
 * @todo Mengambil ruangan yang sesuai dengan nama ruang
 *
 * @example
 * API : GET api/ruang/cari_ruang?nama_ruang=nama_ruang
 * Example : GET api/ruang/cari_ruang?nama_ruang=Gudang
 *
 * @param {String} nama_ruang - nama ruang yang akan dicari
 */

// @desc   Mencari ruangan berdasarkan Inputan (nama)
// @access Public
exports.cariRuang = function(req, res) {
  Ruangan.find({ nama: new RegExp(req.query.nama_ruang, "i") })
    .then(ruang => {
      res.json(ruang);
    })
    .catch(err => {
      res.status(404).json({ msg: "Ruang tidak ditemukan" });
    });
};

/**
 * Menambahkan ruangan
 * @todo menambahkan ruangan baru
 *
 * @example
 * API : POST api/ruang
 * Example : POST api/ruang
 *
 * Dummy POST ( data dapat diubah melalui controller )
 */

// Tambahan
// @desc   Memasukkan ruangan baru
exports.postRuang = function(req, res) {
  const newRuang = new Ruangan({
    nama: "Gudang"
  });

  newRuang
    .save()
    .then(ruang => res.json(ruang))
    .catch(err => {
      res.status(404).json({ msg: "Gagal menyimpan Ruangan" });
    });
};
