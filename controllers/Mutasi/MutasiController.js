// Require  Mutasi models
const Mutasi = require("../../models/Mutasi");
const Barang = require("../../models/Barang");

/**
 * Ambil mutasi
 * @todo Temukan mutasi berdasarkan user
 *
 * @example
 * API : GET api/mutasi/daftar_mutasi
 * Example : GET api/mutasi/daftar_mutasi
 *
 * POSTMAN
 * Body
 * user 5cff29aa9967e3132cee29d8
 *
 * @param {ObjectId} user - Id user yang digunakan sebagai filter mutasi/permohonan
 *
 * @returns {Array} Return data mutasi/permohonan yang user buat
 */

// @desc   Mendapat mutasi barang berdasarkan user id
// @access Private
exports.getMutasi = function(req, res) {
  Mutasi.find({ user: req.headers.user })
    .populate("user")
    .populate("item.barang")
    .then(mutasi => {
      if (mutasi.length == 0) {
        res.json({ msg: "Tidak ada mutasi yang ditemukan " });
      }

      res.json(mutasi);
    })
    .catch(err => {
      res.status(404).json({ msg: "Tidak ada mutasi" });
    });
};

/**
 * Ambil mutasi
 * @todo Temukan detail mutasi berdasarkan id mutasi
 *
 * @example
 * API : GET api/mutasi/daftar_mutasi/:id
 * Example : GET api/mutasi/daftar_mutasi/5cff4c873316ee2a4c5c4061
 *
 * @param {ObjectId} id - Id mutasi yang ingin dilihat detailnya
 *
 * @returns {Object} Return data mutasi yang sesuai dengan id mutasi
 */

// @desc   Mencari mutasi berdasarkan Id
// @access Private
exports.getDetailMutasi = function(req, res) {
  Mutasi.findById(req.params.id)
    .populate("user")
    .populate("item.barang")
    .then(Mutasi => {
      res.json(Mutasi);
    })
    .catch(err => {
      res.status(404).json({ msg: "Mutasi tidak ditemukan" });
    });
};

exports.editDetailMutasi = function(req, res) {
  Mutasi.findOneAndUpdate(
    { _id: req.params.id, "item._id": req.body.id_barang },
    { $set: { "item.$.qty_ambil": req.body.qty_ambil } },
    { new: true },
    function(err, result) {
      if (err) {
        res.json(err);
      }

      res.json(result);
    }
  );
};

/**
 * Tambah mutasi
 * @todo Menambahkan mutasi baru
 *
 * @example
 * API : POST api/mutasi/tambah_mutasi
 * Example : POST api/mutasi/tambah_mutasi
 *
 * POSTMAN
 * Body
 * user        5cff29aa9967e3132cee29d8
 * jumlahItem  1
 * id_barang[] 5cff254caf501920a048b309
 * qty_mohon[] 10
 *
 * @param {ObjectId} user        - Id User yang ingin membuat mutasi/permohonan
 * @param {ObjectId} jumlahItem  - Jumlah barang yang dimohon
 * @param {ObjectId} id_barang[] - Id barang yang ingin dimohon
 * @param {ObjectId} qty_mohon[] - Jumlah barang yang dimohon
 *
 * @returns {Object} Return data permohonan yang berhasil dibuat
 */

// @desc   Membuat mutasi barang
// @access Private
exports.addMutasi = function(req, res) {
  const newMutasi = new Mutasi({
    user: req.body.user
  });

  newMutasi.save().then(mutasi => {
    Mutasi.findById(mutasi.id, async function(err, mutasi) {
      for (a = 0; a < req.body.jumlahItem; a++) {
        const newItem = {
          barang: req.body.id_barang[a],
          qty_mohon: req.body.qty_mohon[a]
        };

        mutasi.item.push(newItem);

        await mutasi.save();
      }

      if (err) {
        res.json(err);
      }

      res.json(mutasi);
    });
  });
};

/**
 * List mutasi
 * @todo Menampilkan list mutasi yang belum terkonfirmasi
 *
 * @example
 * API : GET api/mutasi/list_konfirmasi
 * Example : GET api/mutasi/list_konfirmasi
 *
 * @returns {Array} Return data mutasi/permohonan yang belum terkonfirmasi
 */

// @desc   mendapatkan semua mutasi yang belum terkonfirmasi (petugas)
// @access Private
exports.listMutasi = function(req, res) {
  Mutasi.find({ status: "Menunggu Konfirmasi" })
    .sort({ tgl: -1 })
    .populate("user")
    .populate("item.barang")
    .then(mutasi => res.json(mutasi))
    .catch(err => res.status(404).json({ msg: "Tidak ada request" }));
};

/**
 * Konfirmasi mutasi
 * @todo Mengkonfirmasi mutasi/permohonan
 *
 * @example
 * API : POST api/mutasi/konfirmasi_mutasi/:id
 * Example : POST api/mutasi/konfirmasi_mutasi/5cff4c873316ee2a4c5c4061
 *
 * POSTMAN
 * Body
 * jumlahItem  1
 * id_barang[] 5cff254caf501920a048b309
 * qty_ambil[] 10
 *
 * @param {ObjectId} jumlahItem  - Jumlah barang yang dikonfirmasi
 * @param {ObjectId} id_barang[] - Id barang dari mutasi/permohonan
 * @param {ObjectId} qty_mohon[] - Jumlah barang yang dapat terkonfirmasi
 *
 * @returns {Object} Return data mutasi/permohonan yang telah terkonfirmasi
 */

// @desc   merubah status mutasi menjadi dikonfirmasi dan menentukan jumlah pengembalian oleh operator
// @access Private
exports.konfirmasiMutasi = function(req, res) {
  Mutasi.findById(req.params.id, async function(err, mutasi) {
    for (i = 0; i < req.body.jumlahItem; i++) {
      await Barang.findById(req.body.id_barang[i])
        .then(result => {
          let hasil = result.qty - req.body.qty_ambil[i];

          Barang.findByIdAndUpdate(
            req.body.id_barang[i],
            {
              $set: { qty: hasil }
            },
            function(err, result) {
              if (err) {
                res.json(err);
              }
            }
          );
          // res.json(hasil);
        })
        .catch(err => {
          res.json(err);
        });

      await Mutasi.updateOne(
        { _id: req.params.id, "item.barang": req.body.id_barang[i] },
        { $set: { "item.$.qty_ambil": req.body.qty_ambil[i] } },
        function(err, result) {
          if (err) {
            res.json(err);
          }
        }
      );

      await Mutasi.findByIdAndUpdate(
        req.params.id,
        {
          $set: { status: "Barang Lengkap" }
        },
        function(err, result) {
          if (err) {
            res.json(err);
          }
        }
      ).then(result => {
        res.json(result);
      });
    }
  });
};

/**
 * Konfirmasi penerimaan
 * @todo Mengkonfirmasi penerimaan oleh perawat
 *
 * @example
 * API : POST api/mutasi/konfirmasi_penerimaan
 * Example : POST api/mutasi/konfirmasi_penerimaan
 *
 * POSTMAN
 * Body
 * user        5cff29aa9967e3132cee29d8
 * jumlahItem  1
 * id_barang[] 5cff254caf501920a048b309
 * qty_mohon[] 10
 *
 * @param {ObjectId} user        - Id User yang ingin membuat mutasi/permohonan
 * @param {ObjectId} jumlahItem  - Jumlah barang yang dimohon
 * @param {ObjectId} id_barang[] - Id barang yang ingin dimohon
 * @param {ObjectId} qty_mohon[] - Jumlah barang yang dimohon
 *
 * @returns {Object} Return data permohonan yang berhasil dibuat
 */

// @desc   merubah jumlah penerimaan barang oleh peminjam
// @access Private
exports.konfirmasiPenerimaan = function(req, res) {
  Mutasi.findOneAndUpdate(
    { _id: req.params.mutasi_id, "item._id": req.params.item_id },
    { $set: { "item.$.qty_terima": req.body.qty_terima } },
    { new: true },
    function(err, result) {
      if (err) {
        res.json(err);
      }
      res.json(result);
    }
  );
};

exports.listHistory = function(req, res) {
  Mutasi.find({
    $or: [
      { status: "Barang Lengkap" },
      {
        status: "Selesai"
      }
    ]
  })
    .populate("user")
    .sort({ tgl: -1 })
    .then(result => res.json(result))
    .catch(err => res.json(err));
};
exports.selesaiPermohonan = function(req, res) {
  Mutasi.findOneAndUpdate(
    { _id: req.params.id },
    { $set: { status: "Selesai" } },
    { new: true },
    function(err, result) {
      if (err) {
        res.json(err);
      }
      res.json(result);
    }
  );
};

exports.deletePerhomonan = function(req, res) {
  Mutasi.findByIdAndDelete(req.params.id)
    .then(result => res.json(result))
    .catch(err => res.json(err));
};
