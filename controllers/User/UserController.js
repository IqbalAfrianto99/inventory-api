// Require User models
const User = require("../../models/User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/key");
const passport = require("passport");

// Load input validation
const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");

// @desc Register users
// @access Private

/**
 * Register user
 * @todo Melakukan registerasi user baru
 *
 * @example
 * API : POST api/user/register
 * Example : POST api/user/register/
 *
 * POSTMAN
 * Body
 * email     hendysantoso911@gmail.com
 * nama      hendy
 * password  123456
 *
 * @param {String} email - email user yang akan didaftarkan
 * @param {String} nama  - nama user yang akan didaftarkan
 * @param {String} password - Password dari akun user yang didaftarkan
 * @param {String} password2 - Sebagai parameter untuk mengecheck apakah password sama atau tidak
 *
 * @returns {Object} Return data akun yang baru saja didaftarkan
 */

exports.registerUser = function(req, res) {
  const { errors, isValid } = validateRegisterInput(req.body);

  //Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({
    email: req.body.email
  }).then(user => {
    if (user) {
      errors.email = "Email already exist";
      return res.status(400).json(errors);
    } else {
      const newUser = new User({
        nama: req.body.nama,
        email: req.body.email,
        password: req.body.password,
        role: req.body.role
      });

      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;

          newUser.password = hash;
          newUser
            .save()
            .then(user => res.json(user))
            .catch(err => console.log(err));
        });
      });
    }
  });
};

/**
 * Login user
 * @todo Melakukan login ke halaman
 *
 * @example
 * API : POST api/user/login
 * Example : POST api/user/login
 *
 * POSTMAN
 * Body
 * email     hendysantoso911@gmail.com
 * password  123456
 *
 * @param {String} email - email user yang akan digunakan login
 * @param {String} password - Password dari akun user yang akan digunakan untuk login
 *
 * @returns {Object} Return true beserta token jwt
 */

// @desc   Login user
// @access Public
exports.loginUser = function(req, res) {
  const { errors, isValid } = validateLoginInput(req.body);

  //Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;

  // Find user by email
  User.findOne({
    email: email
  }).then(user => {
    if (!user) {
      errors.email = "User not found";
      errors.success = false;
      return res.status(404).json(errors);
    }

    // check password
    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {
        // User matched

        // Payload
        const payload = {
          id: user.id,
          nama: user.nama,
          role: user.role
        }; // Create jwt payload

        // Sign token
        jwt.sign(
          payload,
          keys.secretOrKey,
          { expiresIn: 3600 },
          (err, token) => {
            if (err) {
              res.json(err);
            }

            res.json({
              user: user,
              success: true,
              token: "Bearer " + token
            });
          }
        );
      } else {
        errors.password = "Password incorrect";
        errors.success = false;
        return res.status(404).json(errors);
      }
    });
  });
};

/**
 * Current user
 * @todo Melakukan pengechekan user
 *
 * @example
 * API : POST api/user/current
 * Example : POST api/user/current
 *
 * POSTMAN
 * Header
 * Authorization Bearer lasklwks@ksdk212kjd122jh31$dooapqweus
 *
 * @param {String} Authorization - Token jwt yang didapatkan saat berhasil login
 *
 * @returns {Object} Return data user yang sesuai dengan token
 */

// @desc   Cek current user by token
// @access Public
exports.currentUser = function(req, res) {
  res.json({
    id: req.user.id,
    name: req.user.name,
    email: req.user.email
  });
};
