const express = require("express");
const router = express.Router();
const passport = require("passport");

/**
 * Route yang berhubungan dengan User
 * @todo Mengatur/menghubungkan data User dari frontend ke backend
 *
 * @example
 *
 * Route List :
 * > POST api/user/register, registerUser
 * > POST api/user/login, loginUser
 * > GET  api/user/current, currentUser
 *
 * Function detail :
 * UserController
 *
 */

// Require user Controller
const UserController = require("../../controllers/User/UserController");

// @Route  POST api/user/register
router.post("/register", UserController.registerUser);

// @Route  POST api/user/login
router.post("/login", UserController.loginUser);

router.get(
  "/current",
  passport.authenticate("jwt", { session: false }),
  UserController.currentUser
);

module.exports = router;
