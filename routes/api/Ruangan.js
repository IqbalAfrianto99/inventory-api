const express = require("express");
const router = express.Router();

const passport = require("passport");
/**
 * Route yang berhubungan dengan Ruang
 * @todo Mengatur/menghubungkan data Ruang dari frontend ke backend
 *
 * @example
 *
 * Route List :
 * > GET  api/ruang, getRuangan
 * > POST api/ruang, postRuang
 * > GET  api/ruang/cari_ruang, cariRuang
 *
 * Function detail :
 * RuanganController
 *
 */

// Require ruangan Controller
const RuanganController = require("../../controllers/Ruangan/RuanganController");

// @Route  GET api/ruang/
router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  RuanganController.getRuangan
);

// @Route  POST api/ruang/
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  RuanganController.postRuang
);

// @Route  GET api/ruang/cari_ruang
router.get(
  "/cari_ruang",
  passport.authenticate("jwt", { session: false }),
  RuanganController.cariRuang
);

module.exports = router;
