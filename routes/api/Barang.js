const express = require("express");
const router = express.Router();
const passport = require("passport");
/**
 * Route yang berhubungan dengan barang
 * @todo Mengatur/menghubungkan data barang dari frontend ke backend
 *
 * @example
 *
 * Route List :
 * > GET  api/barang, getBarang
 * > POST api/barang, postBarang
 * > GET  api/barang/cari_barang, cariBarang
 * > GET  api/barang/detail_barang/:id, getDetailBarang
 *
 * Function detail :
 * BarangController
 *
 */

// Require barang Controller
const BarangController = require("../../controllers/Barang/BarangController");

// @Route  GET api/barang/
router.get(
  "/",
  // passport.authenticate("jwt", { session: false }),
  BarangController.getBarang
);

// @Route  POST api/barang/
router.post(
  "/",
  // passport.authenticate("jwt", { session: false }),
  BarangController.postBarang
);

// @Route  GET api/barang/cari_barang
router.get(
  "/cari_barang",
  // passport.authenticate("jwt", { session: false }),
  BarangController.cariBarang
);

// @Route  GET api/barang/id
router.get(
  "/detail_barang/:id",
  // passport.authenticate("jwt", { session: false }),
  BarangController.getDetailBarang
);

module.exports = router;
