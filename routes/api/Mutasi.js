const express = require("express");
const router = express.Router();

const passport = require("passport");
/**
 * Route yang berhubungan dengan Mutasi
 * @todo Mengatur/menghubungkan data mutasi dari frontend ke backend
 *
 * @example
 *
 * Route List :
 * > GET  api/mutasi/daftar_mutasi, getMutasi
 * > GET  api/mutasi/daftar_mutasi/:id, getDetailMutasi
 * > POST api/mutasi/tambah_mutasi, addMutasi
 * > GET  api/mutasi/list_konfirmasi, listMutasi
 * > GET  api/mutasi/list_konfirmasi/:id, getDetailMutasi
 * > POST api/mutasi/konfirmasi_mutasi/:id, konfirmasiMutasi
 * > POST api/mutasi/konfirmasi_penerimaan/:mutasi_id/:item_id
 *
 * Function detail :
 * MutasiController
 *
 */

// Require barang Controller
const MutasiController = require("../../controllers/Mutasi/MutasiController");

// @Route  GET api/mutasi/daftar_mutasi/
router.get(
  "/daftar_mutasi/",
  // passport.authenticate("jwt", { session: false }),
  MutasiController.getMutasi
);

// @Route  GET api/mutasi/daftar_mutasi/:id
router.get(
  "/daftar_mutasi/:id",
  // passport.authenticate("jwt", { session: false }),
  MutasiController.getDetailMutasi
);

// @Route POST api/mutasi/edit_mutasi/:id
router.post(
  "/edit_mutasi/:id",
  // passport.authenticate("jwt", { session: false }),
  MutasiController.editDetailMutasi
);

// @Route  POST api/mutasi/tambah_mutasi
router.post(
  "/tambah_mutasi",
  // passport.authenticate("jwt", { session: false }),
  MutasiController.addMutasi
);

// @Route GET api/mutasi/list_konfirmasi
router.get(
  "/list_konfirmasi",
  // passport.authenticate("jwt", { session: false }),
  MutasiController.listMutasi
);

// @Route GET api/mutasi/list_konfirmasi/:id
router.get(
  "/list_konfirmasi/:id",
  // passport.authenticate("jwt", { session: false }),
  MutasiController.getDetailMutasi
);

// @Route POST api/mutasi/konfirmasi_mutasi ( Konfirmasi oleh petugas )
router.post(
  "/konfirmasi_mutasi/:id",
  // passport.authenticate("jwt", { session: false }),
  MutasiController.konfirmasiMutasi
);

// @Route POST api/mutasi/konfirmasi_penerimaan/:id ( Konfirmasi oleh peminjam )
router.post(
  "/konfirmasi_penerimaan/:mutasi_id/:item_id",
  // passport.authenticate("jwt", { session: false }),
  MutasiController.konfirmasiPenerimaan
);

// @Route GET api/mutasi/list_history
router.get(
  "/list_history",
  // passport.authenticate("jwt", { session: false }),
  MutasiController.listHistory
);

// @Route POST api/mutasi/selesai_permohonan/:id
router.post(
  "/selesai_permohonan/:id",
  // passport.authenticate("jwt", { session: false }),
  MutasiController.selesaiPermohonan
);

// @Route POST api/mutasi/delete_permohonan/:id
router.delete(
  "/delete_permohonan/:id",
  // passport.authenticate("jwt", { session: false }),
  MutasiController.deletePerhomonan
);

module.exports = router;
