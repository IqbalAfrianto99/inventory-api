FROM node:11-alpine

LABEL authors="Iqbal Afrianto"

# Update & install required packages
#RUN apk add --update nodejs bash git

# Install app dependencies
#COPY package.json /apps/package.json
#RUN cd /apps; npm install

#Copy app sources
#COPY . /apps

#Set working directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY . .

RUN npm install

#COPY . .

#Set your port
ENV PORT 8085

#expose port
EXPOSE 8085

CMD ["npm", "start"] 

